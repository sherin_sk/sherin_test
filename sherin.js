const pgtools = require('pgtools');

const config = {
    user: 'postgres',  // The superuser username
    password: 'your_password', // The superuser password
    port: 5432,  // Default PostgreSQL port
    host: 'localhost' // Default PostgreSQL host
};

const dbname = ''; // Name of the database you want to create
const username = 'your_username'; // Username for the new database
const password = 'your_password'; // Password for the new database

pgtools.createdb(config, dbname, (err, res) => {
    if (err) {
        console.error('Error creating database:', err);
    } else {
        console.log('Database created successfullydgkns');
        // Now you can proceed to create the desired username/password
        pgtools.createUser(config, dbname, username, password, (err, res) => {
            if (err) {
                console.error('Error creating user fd:', err);
            } else {
                console.log('User created successfully');
            }
        });
    }
});
